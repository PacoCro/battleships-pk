<?php

namespace App\ValueObject;

use App\Interfaces\ShipInterface;

final class Battlefield
{
    public const BATTLEFIELD_TYPE_CPU = 'cpu';
    public const BATTLEFIELD_TYPE_PLAYER = 'player';

    public const BATTLEFIELD_WIDTH = 10;
    public const BATTLEFIELD_HEIGHT = 10;

    /**
     * @var ShipInterface[]
     */
    private array $ships;

    /**
     * @param ShipInterface[] $ships
     */
    public function __construct(array $ships)
    {
        $this->ships = $ships;
    }

    /**
     * @return ShipInterface[]
     */
    public function getShips(): array
    {
        return $this->ships;
    }
}
