<?php

namespace App\Exception;

class InvalidShipException extends \Exception
{
    private const MESSAGE = 'Invalid ship.';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
