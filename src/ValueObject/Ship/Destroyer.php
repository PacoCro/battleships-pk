<?php

namespace App\ValueObject\Ship;

class Destroyer extends AbstractShip
{
    private int $size = 4;
    private string $name = 'Destroyer';
}
