<?php

namespace App\ValueObject;

final class Game
{
    private Battlefield $cpuBattlefield;
    private Battlefield $playerBattlefield;

    public function __construct(
        Battlefield $cpuBattlefield,
        Battlefield $playerBattlefield
    ) {
        $this->cpuBattlefield = $cpuBattlefield;
        $this->playerBattlefield = $playerBattlefield;
    }

    public function getCpuBattlefield(): Battlefield
    {
        return $this->cpuBattlefield;
    }

    public function getPlayerBattlefield(): Battlefield
    {
        return $this->playerBattlefield;
    }
}
