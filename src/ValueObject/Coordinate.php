<?php

namespace App\ValueObject;

final class Coordinate
{
    public const VERTICAL_COORDINATE_CHARACTER_MAP = [
        'a' => 1,
        'b' => 2,
        'c' => 3,
        'd' => 4,
        'e' => 5,
        'f' => 6,
        'g' => 7,
        'h' => 8,
        'i' => 9,
        'j' => 10,
    ];

    private int $horizontalPoint;
    private int $verticalPoint;

    public function __construct(
        int $horizontalPoint,
        int $verticalPoint
    ) {
        $this->verticalPoint = $verticalPoint;
        $this->horizontalPoint = $horizontalPoint;
    }

    public function getHorizontalPoint(): int
    {
        return $this->horizontalPoint;
    }

    public function getVerticalPoint(): int
    {
        return $this->verticalPoint;
    }

    public function isEqualTo(Coordinate $coordinate): bool
    {
        if (
            $this->getHorizontalPoint() === $coordinate->getHorizontalPoint() &&
            $this->getVerticalPoint() === $coordinate->getVerticalPoint()
        ) {
            return true;
        }

        return false;
    }
}
