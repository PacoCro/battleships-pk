<?php

namespace App\Service;

use App\ValueObject\Coordinate;
use App\ValueObject\Direction;

class RandomShipCoordinateGenerator
{
    private ShipCoordinateFinder $shipCoordinateFinder;

    public function __construct(ShipCoordinateFinder $shipCoordinateFinder)
    {
        $this->shipCoordinateFinder = $shipCoordinateFinder;
    }

    /**
     * @return Coordinate[]
     */
    public function generateShipCoordinates(
        int $shipSize
    ): array {
        $coordinates = [];

        $firstCoordinate = $this->getRandomFirstCoordinate();
        $direction = $this->getRandomDirection($firstCoordinate, $shipSize);

        return $this->shipCoordinateFinder->getShipCoordinates(
            $firstCoordinate,
            $direction,
            $shipSize
        );
    }

    private function getRandomFirstCoordinate(): Coordinate
    {
        return new Coordinate(rand(1, 10), rand(1, 10));
    }

    private function getRandomDirection(
        Coordinate $firstCoordinate,
        int $shipSize
    ): string {
        // TODO: Filter direction map only for valid directions
        return Direction::MAP[array_rand(Direction::MAP)];
    }
}
