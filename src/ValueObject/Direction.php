<?php

namespace App\ValueObject;

class Direction
{
    public const LEFT = 'left';
    public const RIGHT = 'right';
    public const UP = 'up';
    public const DOWN = 'down';

    public const MAP = [
        self::LEFT,
        self::RIGHT,
        self::UP,
        self::DOWN
    ];
}
