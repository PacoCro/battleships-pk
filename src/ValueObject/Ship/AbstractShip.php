<?php

namespace App\ValueObject\Ship;

use App\Interfaces\ShipInterface;
use App\ValueObject\Coordinate;

abstract class AbstractShip implements ShipInterface
{
    /**
     * Default size. Should be overridden in child class.
     */
    private int $size = 1;

    /**
     * Default name. Should be overridden in child class.
     */
    private string $name = 'Default name';

    /**
     * @var Coordinate[]
     */
    protected array $coordinates = [];

    public function getSize(): int
    {
        return $this->getSize();
    }

    public function getName(): string
    {
        return $this->getName();
    }

    public function isHit(Coordinate $coordinate): bool
    {
        foreach ($this->coordinates as $shipCoordinate) {
            if ($shipCoordinate->isEqualTo($coordinate)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Coordinate[]
     */
    public function getCoordinates(): array
    {
        return $this->coordinates;
    }

    /**
     * @param Coordinate[] $coordinates
     */
    public function setCoordinates(array $coordinates): void
    {
        $this->coordinates = $coordinates;
    }
}
