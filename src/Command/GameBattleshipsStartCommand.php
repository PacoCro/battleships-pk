<?php

namespace App\Command;

use App\Factory\GameFactory;
use App\Service\ShipCoordinateFinder;
use App\ValueObject\Battlefield;
use App\ValueObject\Direction;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class GameBattleshipsStartCommand extends Command
{
    protected static $defaultName = 'game:battleships:start';
    protected static $defaultDescription = 'Starts a new battleship game.';

    private SymfonyStyle $io;
    private ShipCoordinateFinder $shipCoordinateFinder;
    private GameFactory $gameFactory;

    public function __construct(
        GameFactory $gameFactory,
        ShipCoordinateFinder  $shipCoordinateFinder
    ) {
        $this->shipCoordinateFinder = $shipCoordinateFinder;
        $this->gameFactory = $gameFactory;

        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        $game = $this->gameFactory->buildGame();
        $playerBattlefield = $game->getPlayerBattlefield();

        $this->getShipDataFromPlayer($playerBattlefield);

        // TODO: Play the game, ask player for first coordinate...

        $this->io->success('Game over.');

        return Command::SUCCESS;
    }

    private function getShipDataFromPlayer(Battlefield $playerBattlefield): void
    {
        $shipCounter = 0;
        foreach ($playerBattlefield->getShips() as $ship) {
            $shipCounter++;

            $coordinate = null;
            while ($coordinate === null) {
                $shipCoordinateQuestion = $this->getCoordinateQuestion(
                    $shipCounter,
                    $ship->getName(),
                    $ship->getSize()
                );

                $coordinate = $this->io->askQuestion($shipCoordinateQuestion);
            }

            $shipDirectionQuestion = $this->getDirectionQuestion($shipCounter);
            $direction = $this->io->askQuestion($shipDirectionQuestion);

            $shipCoordinates = $this->shipCoordinateFinder->getShipCoordinates(
                $coordinate,
                $direction,
                $ship->getSize()
            );

            $ship->setCoordinates($shipCoordinates);
        }
    }

    private function getCoordinateQuestion(
        int $counter,
        string $name,
        int $size
    ): Question {
        return new Question(
            'Ship number '  .
            $counter .
            ' (' .
            $name .
            ' - size ' .
            $size.
            ') Add start coordinate (e.g. A5)'
        );
    }

    private function getDirectionQuestion(int $counter): Question
    {
        return new ChoiceQuestion(
            'Pick ship number '  .
            $counter .
            'direction:',
            Direction::MAP
        );
    }
}
