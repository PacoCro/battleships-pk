<?php

namespace App\Service;

use App\ValueObject\Coordinate;
use App\ValueObject\Direction;

class ShipCoordinateFinder
{
    /**
     * @return Coordinate[]
     */
    public function getShipCoordinates(
        Coordinate $firstCoordinate,
        string $direction,
        int $shipSize
    ): array {
        $shipCoordinates = [
            $firstCoordinate
        ];

        $currentCoordinate = $firstCoordinate;
        while ($shipSize >= 0) {
            $currentCoordinate = $this->getNextCoordinate($currentCoordinate, $direction);
            $shipCoordinates[] = $currentCoordinate;
        }

        return $shipCoordinates;
    }

    private function getNextCoordinate(
        Coordinate $currentCoordinate,
        string $direction
    ): Coordinate {
        return new Coordinate(
            $this->getHorizontalPoint($currentCoordinate->getHorizontalPoint(), $direction),
            $this->getVerticalPoint($currentCoordinate->getVerticalPoint(), $direction)
        );
    }

    private function getHorizontalPoint(int $horizontalPoint, string $direction): int
    {
        if (!in_array($direction, [Direction::RIGHT, Direction::LEFT])) {
            return $horizontalPoint;
        }

        // TODO: Validate if point is within boundaries of battlefield
        // TODO: Calculate next point
        return 1;
    }

    private function getVerticalPoint(int $verticalPoint, string $direction): int
    {
        if (!in_array($direction, [Direction::UP, Direction::DOWN])) {
            return $verticalPoint;
        }

        // TODO: Validate if point is within boundaries of battlefield
        // TODO: Calculate next point
        return 1;
    }
}
