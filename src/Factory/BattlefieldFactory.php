<?php

namespace App\Factory;

use App\Exception\UnsupportedBattlefieldTypeException;
use App\Interfaces\ShipInterface;
use App\Service\RandomShipCoordinateGenerator;
use App\ValueObject\Battlefield;
use App\ValueObject\Ship\Battleship;
use App\ValueObject\Ship\Destroyer;

class BattlefieldFactory
{
    private RandomShipCoordinateGenerator $randomShipCoordinateGenerator;

    private ShipFactory $shipFactory;

    public function __construct(
        RandomShipCoordinateGenerator $randomShipCoordinateGenerator,
        ShipFactory $shipFactory
    ) {
        $this->randomShipCoordinateGenerator = $randomShipCoordinateGenerator;
        $this->shipFactory = $shipFactory;
    }

    public function createBattlefield(string $battlefieldType): Battlefield
    {
        $ships = $this->getDefaultShipFleet();

        switch ($battlefieldType) {
            case Battlefield::BATTLEFIELD_TYPE_PLAYER:
                return new Battlefield($ships);
            case Battlefield::BATTLEFIELD_TYPE_CPU:
                $cpuBattlefield = new Battlefield($ships);

                foreach ($cpuBattlefield->getShips() as $ship) {
                    $ship->setCoordinates(
                        $this->randomShipCoordinateGenerator->generateShipCoordinates($ship->getSize())
                    );
                }

                return $cpuBattlefield;
            default:
                throw new UnsupportedBattlefieldTypeException();
        }
    }

    /**
     * @return ShipInterface[]
     * @throws \App\Exception\InvalidShipException
     */
    private function getDefaultShipFleet(): array
    {
        return [
            $this->shipFactory->createShip(Destroyer::class),
            $this->shipFactory->createShip(Destroyer::class),
            $this->shipFactory->createShip(Battleship::class)
        ];
    }
}
