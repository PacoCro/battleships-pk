<?php

namespace App\Factory;

use App\ValueObject\Battlefield;
use App\ValueObject\Game;

class GameFactory
{
    private BattlefieldFactory  $battlefieldFactory;

    public function __construct(BattlefieldFactory $battlefieldFactory)
    {
        $this->battlefieldFactory = $battlefieldFactory;
    }

    public function buildGame(): Game
    {
        return new Game(
            $this->battlefieldFactory->createBattlefield(Battlefield::BATTLEFIELD_TYPE_CPU),
            $this->battlefieldFactory->createBattlefield(Battlefield::BATTLEFIELD_TYPE_PLAYER)
        );
    }
}
