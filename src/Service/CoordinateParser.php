<?php

namespace App\Service;

use App\Exception\UnparsableCoordinateException;
use App\ValueObject\Coordinate;

class CoordinateParser
{
    public function parse(string $text): Coordinate
    {
        // TODO: Implement more specific coordinate validation
        if (strlen($text) > 2) {
            throw new UnparsableCoordinateException();
        }

        $horizontalPoint = (int) substr($text, 1, 1);
        $verticalPoint = Coordinate::VERTICAL_COORDINATE_CHARACTER_MAP[substr($text, 0, 1)];

        return new Coordinate(
            $horizontalPoint,
            $verticalPoint
        );
    }
}
