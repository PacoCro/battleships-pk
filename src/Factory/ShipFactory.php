<?php

namespace App\Factory;

use App\Exception\InvalidShipException;
use App\Interfaces\ShipInterface;

class ShipFactory
{
    public function createShip(string $shipClassName): ShipInterface
    {
        $ship = new $shipClassName();

        if (!$ship instanceof ShipInterface) {
            throw new InvalidShipException();
        }

        return $ship;
    }

}
