<?php

namespace App\Exception;

class UnsupportedBattlefieldTypeException extends \Exception
{
    private const MESSAGE = 'Unsupported battlefield type.';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
