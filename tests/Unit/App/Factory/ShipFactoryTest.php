<?php

namespace App\Tests\Unit\App\Factory;

use App\Exception\InvalidShipException;
use App\Factory\ShipFactory;
use App\Interfaces\ShipInterface;
use App\ValueObject\Ship\Destroyer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ShipFactoryTest extends KernelTestCase
{
    private ShipFactory $shipFactory;

    protected function setUp(): void
    {
        $this->shipFactory = new ShipFactory();

        parent::setUp();
    }

    public function test_create_ship(): void
    {
        $destroyerShip = $this->shipFactory->createShip(Destroyer::class);

        $this->assertTrue($destroyerShip instanceof ShipInterface);
    }

    public function test_create_ship_invalid_ship_class(): void
    {
        $this->expectException(InvalidShipException::class);

        $this->shipFactory->createShip(\stdClass::class);
    }
}
