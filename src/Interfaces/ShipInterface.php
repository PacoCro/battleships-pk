<?php

namespace App\Interfaces;

use App\ValueObject\Coordinate;

interface ShipInterface
{
    public function getSize(): int;
    public function getName(): string;

    public function isHit(Coordinate $coordinate): bool;

    /**
     * @return Coordinate[]
     */
    public function getCoordinates(): array;

    /**
     * @param Coordinate[] $coordinates
     *
     * @return void
     */
    public function setCoordinates(array $coordinates): void;
}
