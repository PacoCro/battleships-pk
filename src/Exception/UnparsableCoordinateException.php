<?php

namespace App\Exception;

class UnparsableCoordinateException extends \Exception
{
    private const MESSAGE = 'Unparsable coordinate.';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
