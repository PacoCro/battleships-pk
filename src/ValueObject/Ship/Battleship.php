<?php

namespace App\ValueObject\Ship;

class Battleship extends AbstractShip
{
    private int $size = 5;
    private string $name = 'Battleship';
}
